package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Hypers
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageViewCount {

    private String url;
    private Long windowEnd;
    private Long count;
}
